#include <sourcemod>
#include <admin>
#include <adt_array>
#include <colors>

#define PLAYER_ENV_ATTRIBUTES_BHOP 1
#define PLAYER_ENV_ATTRIBUTES_SURF 2
#define PLAYER_ENV_ATTRIBUTES_AUTOBHOP 4
#define PLAYER_ENV_ATTRIBUTES_CSGOMOVEMENT 8
#define PLAYER_ENV_ATTRIBUTES_CSGODUCKHULL 16



// plugin info
public Plugin myinfo =
{
	name = "EAC",
	author = "Menko",
	description = "Ethmo Anti Cheat",
	version = "0.4",
	url = "etherealmovement.eu"
};

const int INT_MAX = 2147483647 - 1;
const int SIZE_OF_SECTION = 40;

// globals
int g_FrameCount;

//// detection methods
// Frame Method
bool g_FrameMethod; // turn method on/off
int g_PlayerJumpedFrame[MAXPLAYERS];
int g_PlayerLandedFrame[MAXPLAYERS];
bool g_ShouldPrintSection;
int g_AnalyzeSection[MAXPLAYERS][SIZE_OF_SECTION];
int g_CurrentSectionElements[MAXPLAYERS];
bool g_PrevOnGround[MAXPLAYERS];
////


public void OnPluginStart() {
    PrintToServer("EAC System");
    RegAdminCmd("eac_frame_method", command_FrameMethod, ADMFLAG_GENERIC , "Enables Frame Method Detection");
    //TODO
    //RegAdminCmd("eac_frame_method_section", command_FrameMethodSection, Admin_Generic, "Sets Size Of Section to be Analyzed"); 
    RegAdminCmd("eac_print_frame_section_toggle", command_PrintFrameSection, ADMFLAG_GENERIC , "Should section data be printed to console");

    g_FrameCount = 0;
    g_FrameMethod = false;
    g_ShouldPrintSection = false;
    for(int i = 0; i < MAXPLAYERS; i++){
        g_CurrentSectionElements[i] = 0;
        g_PrevOnGround[i] = true;
    }

}

// Actions
public Action OnPlayerRunCmd(int client, int& buttons, int& impulse, float vel[3], float angles[3], int& weapon, int& subtype, int& cmdnum, int& tickcount, int& seed, int mouse[2]) {
    
    g_FrameCount++;
    if(g_FrameCount >= INT_MAX){
        g_FrameCount = 0;
    }

    int p_Flags = GetEntityFlags(client);

    int p_Mode = GetEntProp(client, Prop_Send, "m_iEnvironmentalAttributes");

     //  disabled for testing
    if(g_FrameMethod  && !IsFakeClient(client) && !(p_Mode & PLAYER_ENV_ATTRIBUTES_AUTOBHOP)){

        bool p_CurrentOnGround = (p_Flags & FL_ONGROUND);
        bool p_Jumped = false;

        if(g_PrevOnGround[client] && !p_CurrentOnGround) {
            g_PlayerJumpedFrame[client] = g_FrameCount;
            p_Jumped = true; 
        }else if(!g_PrevOnGround[client] && p_CurrentOnGround) {
            g_PlayerLandedFrame[client] = g_FrameCount;
        }

        g_PrevOnGround[client] = p_CurrentOnGround;

        if(!p_Jumped) {
            return Plugin_Continue;
        }

        if(g_PlayerJumpedFrame[client] < g_PlayerLandedFrame[client]) {
            return Plugin_Continue;
        }

        int p_Difference = g_PlayerJumpedFrame[client] - g_PlayerLandedFrame[client];
        if (p_Difference > 6) {
            return Plugin_Continue;
        }

        if(g_CurrentSectionElements[client] > SIZE_OF_SECTION - 1) {
            AnalyzeFrames(client);
        }
        g_AnalyzeSection[client][g_CurrentSectionElements[client]] = p_Difference;
        g_CurrentSectionElements[client]++;
        return Plugin_Continue;
    }
    return Plugin_Continue;
}

public void AnalyzeFrames(int client) {

    char p_ClientName[32];
    GetClientName(client, p_ClientName, sizeof(p_ClientName));

    int p_NaturalFrames = GetNaturalFrames(g_AnalyzeSection[client]);
    float p_NaturalPercentage = GetNaturalPercentage(g_AnalyzeSection[client]);
    float p_FirstFramesPercentage = GetFirstFrames(g_AnalyzeSection[client]);

    // check if cheating
    if(p_FirstFramesPercentage > 95.0 && p_NaturalFrames < 2){
        CPrintToChatAll("EAC: {red}CHEAT DETECTED [NF %d| FF %.2f| NP %.2f].{default} Player -[%s] - {green}Analyzed {default}[%d] {green}segments", 
                        p_NaturalFrames, p_FirstFramesPercentage, p_NaturalPercentage,  p_ClientName, g_CurrentSectionElements[client]);
    }else if(p_NaturalFrames < 2 && p_FirstFramesPercentage > 70.0) {
        CPrintToChatAll("EAC: {blue}POSSIBLE CHEAT [NF %d| FF %.2f| NP %.2f].{default} Player -[%s] - {green}Analyzed {default}[%d] {green}segments", 
                        p_NaturalFrames, p_FirstFramesPercentage, p_NaturalPercentage,  p_ClientName, g_CurrentSectionElements[client]);
    }else if(p_NaturalFrames < 2 && p_FirstFramesPercentage > 55.0){
        CPrintToChatAll("EAC: {olive}[NF %d| FF %.2f| NP %.2f].{default} Player -[%s] - [%d]SEG", 
                        p_NaturalFrames, p_FirstFramesPercentage, p_NaturalPercentage,  p_ClientName, g_CurrentSectionElements[client]);
    }

    if(g_ShouldPrintSection){
        PrintToConsoleAll("EAC: SECTION DATA FOR PLAYER - %s", p_ClientName);
        for(int i = 0; i < SIZE_OF_SECTION; i++) {
            PrintToConsoleAll("%d", g_AnalyzeSection[client][i]);
        }
        PrintToConsoleAll("EAC: END SECTION");
    }
    for(int i = 0; i < SIZE_OF_SECTION; i++){
        g_AnalyzeSection[client][i] = 0;
    }
    g_CurrentSectionElements[client] = 0;
}

public float GetFirstFrames(int aoArray[SIZE_OF_SECTION]) {
    float p_Frames = 0.0;
    float p_Length = 40.0;
    for(int i = 0; i < SIZE_OF_SECTION; i++) {
        if(aoArray[i] < 2) {
            p_Frames += 1.0;
        }
    }

    return p_Frames/p_Length * 100.0;
}

public int GetNaturalFrames(int aoArray[SIZE_OF_SECTION]) {  
    int p_Naturals = 0;
    for(int i = 0; i < SIZE_OF_SECTION; i++) {
        if(aoArray[i] > 2) {
            p_Naturals++;
        }
    }
    return p_Naturals;
}

public float GetNaturalPercentage(int aoArray[SIZE_OF_SECTION]) {
    float p_Naturals = 0.0;
    float p_Length = 40.0
    for(int i = 0; i < SIZE_OF_SECTION; i++) {
        if(aoArray[i] > 2) {
            p_Naturals += 1.0;
        }
    }
    return p_Naturals/p_Length * 100.0;
}

// commands
public Action command_FrameMethod(client, args) {
    if(args < 1) {
        int p_CurrentState = 0;
        if(g_FrameMethod) {
            p_CurrentState = 1;
        }
        PrintToConsole(client, "EAC: Argument not specified. Current value - %d", p_CurrentState);
        return Plugin_Continue;
    }
    char p_Arg[8];
    GetCmdArg(1, p_Arg, sizeof(p_Arg));
    
    if(StrEqual(p_Arg[0], "1")) {
        g_FrameMethod = true;
        return Plugin_Continue;
    }
    g_FrameMethod = false;
    return Plugin_Continue;
}

public Action command_PrintFrameSection(client, args) {
    g_ShouldPrintSection = !g_ShouldPrintSection;
}